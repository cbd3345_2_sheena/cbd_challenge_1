FROM python:3.10-alpine

WORKDIR /

COPY . .

RUN pip install --no-cache-dir --upgrade pip
RUN pip install --no-cache-dir -r requirements.txt

EXPOSE 6001

CMD ["python", "-u", "main.py"]

