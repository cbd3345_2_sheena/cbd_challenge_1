import json

from app import app
import os
from dotenv import load_dotenv
from pathlib import Path
from flask import request, jsonify

d = Path(__file__).resolve().parents[1]
load_dotenv(rf"{d}/.env")


def create_db_client():
    database_password = os.environ.get("DATABASE_PASSWORD")
    database_username = os.environ.get("DATABASE_USER")
    database_url = os.environ.get("DATABASE_URL")
    print(database_username)
    print(database_password)
    print(database_url)
    return None


db_client = create_db_client()


@app.route("/", methods=["GET"])
def index():
    return "Welcome Sheena Dumbrique! You are authenticated to use the API."


@app.route("/database")
def database():
    database_password = os.environ.get("DATABASE_PASSWORD")
    database_username = os.environ.get("DATABASE_USER")
    database_url = os.environ.get("DATABASE_URL")
    # return database_url, database_username, database_password
    data = {
        "CLUSTER URL": database_url,
        "USERNAME": database_username,
        "PASSWORD": database_password,
        "DATA": {
            "age": 50,
            "height": 182,
            "Weight": 85
        }
    }
    return jsonify(data)
